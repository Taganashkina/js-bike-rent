'use strict';
function init() {
  // вызови функцию loadCatalog для загрузки первой страницы каталога

  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
  let page = 1
  loadCatalog(page)

  document.getElementById('loadMore').addEventListener('click', () => {
    page++
    loadCatalog(page)
  })

}

function loadCatalog(page) {
  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore
  disableButtonLoadMore()
  const pointId = getPointId()
  const response = api.getBikes(pointId, page)
  response.then((value) => {
    showButtonLoadMore(value.hasMore)
    appendCatalog(value.bikesList)
    enableButtonLoadMore()
  })

}

function appendCatalog(items) {
  // отрисуй велосипеды из items в блоке <div id="bikeList">
  items.forEach(item => {
    let cardImg = document.createElement('div')
    cardImg.classList.add('cardImg')
    cardImg.style.backgroundImage = `url('../images/${item.img}')`

    let cardName = document.createElement('p')
    cardName.classList.add('name')
    cardName.textContent = `${item.name}`

    let cardCost = document.createElement('p')
    cardCost.classList.add('cost')
    cardCost.textContent = `Стоимость за час - ${item.cost} ₽`

    let cardBtn = document.createElement('button')
    cardBtn.classList.add('button', 'rentBtn')
    cardBtn.textContent = 'Арендовать'

    let card = document.createElement('div')
    card.classList.add('item')
    card.append(cardImg, cardName, cardCost, cardBtn)

    let bikeList = document.getElementById('bikeList')
    bikeList.appendChild(card)
  });

}

function showButtonLoadMore(hasMore) {
  // если hasMore == true, то показывай кнопку #loadMore
  // иначе скрывай
  if (hasMore) {
    document.getElementById('loadMore').classList.remove('hidden')
    document.querySelector('.loadMoreBtn').classList.remove('hidden')
  } else {
    document.querySelector('.loadMoreBtn').classList.add('hidden')
    document.getElementById('loadMore').classList.add('hidden')
  }

}

function disableButtonLoadMore() {
  // заблокируй кнопку "загрузить еще"
  document.getElementById('loadMore').removeAttribute('disabled')
}

function enableButtonLoadMore() {
  // разблокируй кнопку "загрузить еще"
  document.getElementById('loadMore').removeAttribute('disabled')
}

function getPointId() {
  // сделай определение id выбранного пункта проката
  let urlArray = document.URL.split('/')
  return urlArray[urlArray.length - 2] === 'catalog' ? urlArray[urlArray.length - 1] : "false"

}


document.addEventListener('DOMContentLoaded', init)
